<?php
class Product extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    #echo  phpinfo();
    #$this->load->helper('url');
    $this->load->helper('url');
    $this->load->database();
  }

  public function index()
  {
    $query = $this->db->get("products");
    $data['productList'] = $query->result();
    $this->load->view('Product_index' , $data);
  }

  public function new()
  {
    $this->load->helper('form');
    $this->load->view('Product_new');
  }

  public function create()
  {
    $this->load->model('Product_model');
    $data = array( 'name' => $this->input->post('txtName'), 'price' => $this->input->post('txtPrice') );
    $this->Product_model->save($data);
    $query = $this->db->get("products");
    $data['productList'] = $query->result();
    $this->load->view('Product_index',$data);
  }



  public function edit()
  {
         $this->load->helper('form');
         $idParam = $this->uri->segment('3');
         $query = $this->db->get_where("products",array("id"=>$idParam));
         $data['productList'] = $query->result();
         $data['old_id'] = $idParam;
         $this->load->view('Product_edit',$data);
  }

  public function update()
  {

        $this->load->model('Product_model');

        $data = array(
           'id' => $this->input->post('txtId'),
           'name' => $this->input->post('txtName'),
           'price' => $this->input->post('txtPrice')
        );

        $old_id = $this->input->post('old_id');
        $this->Product_model->update($data,$old_id);

        $query = $this->db->get("products");
        $data['productList'] = $query->result();
        $this->load->view('Product_index',$data);
   }

  public function delete()
  {
         $this->load->model('Product_model');
         $id = $this->uri->segment('3');
         $this->Product_model->delete($id);
         $query = $this->db->get("products");
         $data['productList'] = $query->result();
         $this->load->view('Product_index',$data);
  }






public function methodHttpTryDelete()
{
  $response = [];
  $selectedIdParam = intval($_POST['selectedIdParam']);
  $query = $this->db->get_where("products",array("id"=>$selectedIdParam));
  $result = $query->result();
  foreach ($result as $value) {
    $response['name'] = $value->name;
    $response['price'] = $value->price;
  }
  echo json_encode($response);
}

public function methodHttpConfirmDelete()
{
  $this->load->model('Product_model');
  $selectedIdParam = intval($_POST['selectedIdParam']);
  $result =  $this->Product_model->delete($selectedIdParam);
  if ($result) {
    echo 'deleted';
  }
  else
  {
    echo "error";
  }
}



public function methodHttpNewOrEdit()
{
  $response = [];
  if ($_POST['selectedActionParam'] =='new')
  {

  }
  elseif ($_POST['selectedActionParam'] == 'edit')
  {
    $selectedIdParam = intval($_POST['selectedIdParam']);
    $query = $this->db->get_where("products",array("id"=>$selectedIdParam));
    $result = $query->result();
    foreach ($result as $value) {
      $response['name'] = $value->name;
      $response['price'] = $value->price;
    }
  }
  echo json_encode($response);
}





  public function methodHttpCreateOrUpdate()
  {
      if ($_POST['selectedActionParam'] == 'new')
       {
        $this->load->model('Product_model');
        $data = array(
          'name'=>$_POST['name'],
          'price'=>$_POST['price'],
        );
        $result = $this->Product_model->save($data);
        if ($result)
        {
          echo 'created';
        }
        else
        {
          echo "error";
        }
      }

      elseif ($_POST['selectedActionParam'] =='edit')
      {
        $this->load->model('Product_model');
        $selectedIdParam = intval($_POST['selectedIdParam']);
        $data = array(
          'name'=>$_POST['name'],
          'price'=>$_POST['price'],
        );
        $result = $this->Product_model->update($data,$selectedIdParam);
        if ($result) {
          echo 'updated';
        }
        else
        {
          echo "error";
        }
      }
}



}
?>
