<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <title>Productos</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  </head>


  <body>
    <div class="container">
        <br />
        <br />
        <a  class='btn btn-success btn-sm' href = "<?php echo base_url();?>index.php/product/new">Nuevo producto</a>

        <br />
        <br />


      <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal" id="btnNewInModal">
        Nuevo producto con modal
      </button>


        <hr />

        <div class="row">
          <div class="col">
            <table border="1" class="table">
              <?php
                $i=1;
                echo "<thead class='thead-dark'>";
                  echo "<tr>";
                    echo "<td class='text-center'>#</td>";
                    echo "<td>Nombre</td>";
                    echo "<td>Precio</td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "</tr>";
                echo "</thead>";

                echo "<tbody>";
                  foreach($productList as $r) {
                         echo "<tr>";
                         echo "<td class='text-center'>".$i++."</td>";
                         echo "<td>".$r->name."</td>";
                         echo "<td class='text-right'>".$r->price."</td>";

                         echo "<td class='text-center'><a class='btn btn-info btn-sm' href = '".base_url()."index.php/product/edit/".$r->id."'>Editar</a></td>";
                         echo "<td><button type='button' class='btn btn-info btn-sm' id='btnEditInModal' data-idProductParam='$r->id'>Editar con modal</button></td>";

                         echo "<td class='text-center'><a class='btn btn-danger btn-sm' href = '".base_url()."index.php/product/delete/".$r->id."'>Eliminar</a></td>";
                          echo "<td><button type='button' class='btn btn-danger btn-sm' id='btnDeleteInModal' data-idProductParam='$r->id'>Eliminar con modal</button></td>";
                         echo "<tr>";
                  }
                echo "</tbody>";
               ?>
            </table>
          </div>
        </div>
      </div>




      <div class="modal fade" id="myModal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">


            <div class="modal-header">
              <h5 class="modal-title" id="myModalTitle">Nuevo producto</h5>

              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form id="formModal">
              <div class="modal-body">
                <div class="form-group">
                  <label class="control-label">Nombre</label>
                  <input type="text" class="form-control" name="txtName" id="txtName" required>
                </div>

                <div class="form-group">
                  <label class="control-label">Precio</label>
                  <input type="number" class="form-control" name="txtPrice" id="txtPrice" required step="0.01">
                </div>
              </div>
              <div class="modal-footer">
                <input type="hidden"  name="selectedAction"  id="selectedAction" value="new">
                <input type="hidden"  name="selectedId"      id="selectedId"     value="0">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <input type="submit"  name="submit" id="submit" class="btn btn-primary" value="Guardar">
              </div>
            </form>

          </div>
        </div>
      </div>



      <div class="modal fade" id="myModalDelete" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">


            <div class="modal-header">
              <h5 class="modal-title" id="myModalDeleteTitle">Eliminar producto</h5>

              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form id="formModalDelete">
              <div class="modal-body">
                <div class="form-group">
                  <label class="control-label">Nombre</label>
                  <input type="text" class="form-control" name="txtNameDel" id="txtNameDel" required disabled>
                </div>

                <div class="form-group">
                  <label class="control-label">Precio</label>
                  <input type="number" class="form-control" name="txtPriceDel" id="txtPriceDel" required step="0.01" disabled>
                </div>
              </div>
              <div class="modal-footer">
                <input type="hidden"  name="selectedId"      id="selectedId"     value="0">
                <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                <input type="submit"  name="submit" id="submit" class="btn btn-danger" value="Estoy de acuerdo">
              </div>
            </form>

          </div>
        </div>
      </div>



  </body>







  <script type='text/javascript' src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
  <script type='text/javascript' src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

  <script type="text/javascript">









  $(document).on("click","#btnDeleteInModal",function(e)
  {
    e.preventDefault();
    var selectedIdParam = $(this).attr('data-idProductParam');
    $("#selectedId").val(selectedIdParam);
    $("#myModalDeleteTitle").text('¿Esta de acuerdo en eliminar el siguiente producto?');
    $.ajax({
      method: 'POST',
      url:'product/methodHttpTryDelete',
      data:
      {
        selectedIdParam:selectedIdParam,
      },
       dataType: 'json',
       success: function(data)
       {
         $("#txtNameDel").val(data.name);
         $("#txtPriceDel").val(data.price);
        $("#myModalDelete").modal('show');
       },
       error: function(error_data)
       {
        console.log(JSON.stringify(error_data))
       }
    })

  });
  $(document).on("submit","#formModalDelete",function(e)
  {
    e.preventDefault();
    var selectedIdParam     = $("#selectedId").val();
    $.ajax({
      method: 'POST',
      url:'product/methodHttpConfirmDelete',
      data:
      {
        selectedIdParam:selectedIdParam,
      },
       success: function(data)
       {
         console.log('Response :: ' + data)
         if (data.trim()=='deleted')
         {
           $("#myModalDelete").modal('hide');
           alert('Se eliminó el producto')
         }
         location.reload();
       },
       error: function(error_data)
       {
        console.log(JSON.stringify(error_data))
       }
    })
  });










  $(document).on("click","#btnNewInModal",function(e)
  {
    e.preventDefault();
    $("#myModalTitle").text('NUEVO PRODUCTO');
    $("#selectedAction").val('new');
    $("#formModal")[0].reset();
    $("#myModal").modal('show');
  });


  $(document).on("click","#btnEditInModal",function(e)
  {
    e.preventDefault();
    $("#myModalTitle").text('EDITAR PRODUCTO');

    $("#selectedAction").val('edit');
    var selectedActionParam = $("#selectedAction").val();

    var selectedIdParam = $(this).attr('data-idProductParam');
    $("#selectedId").val(selectedIdParam);



    $.ajax({
      method: 'POST',
      url:'product/methodHttpNewOrEdit',
      data:
      {
        selectedIdParam:selectedIdParam,
        selectedActionParam:selectedActionParam
      },
       dataType: 'json',
       success: function(data)
       {
         //console.log(JSON.stringify(data))
         $("#txtName").val(data.name);
         $("#txtPrice").val(data.price);

          $("#myModal").modal('show');
       },
       error: function(error_data)
       {
        console.log(JSON.stringify(error_data))
       }
    })
  });





   $(document).on("submit","#formModal",function(e)
   {
     e.preventDefault();
     var nameParam = $("#txtName").val();
     var priceParam = $("#txtPrice").val();
     var selectedIdParam     = $("#selectedId").val();
     var selectedActionParam = $("#selectedAction").val();
     $.ajax({
       method: 'POST',
       url:'product/methodHttpCreateOrUpdate',
       data:
       {
         selectedIdParam:selectedIdParam,
         selectedActionParam:selectedActionParam,
         name:nameParam,
         price:priceParam,
       },
        //dataType: 'json',
        //contentType:false,
        //processData:false,
        success: function(data)
        {
          console.log('Response :: ' + data)
          if (data.trim()=='updated')
          {
            $("#myModal").modal('hide');
            alert('Actualización exitósa')
          }
          else if (data.trim()=='created')
          {
            $("#myModal").modal('hide');
            alert('El producto se ha registrado')
          }
          location.reload();

        },
        error: function(error_data)
        {
         console.log(JSON.stringify(error_data))
        }
     })
   });


  </script>
</html>
