<!DOCTYPE html>
<html lang = "en">

   <head>
      <meta charset = "utf-8">
      <title>Nuevo producto</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

   </head>
   <body>
       <a href = "<?php echo base_url(); ?>index.php/product">Ver todos</a>
       <hr />
         <?php
            echo form_open('Product/create');
              echo form_label('Nombre');
              echo form_input(array('id'=>'txtName','name'=>'txtName'));
              echo "<br/>";

              echo form_label('Precio');
              echo form_input(array('id'=>'txtPrice','name'=>'txtPrice'));
              echo "<br/>";

              echo form_submit(array('id'=>'submit','value'=>'Guardar'));
            echo form_close();
         ?>
   </body>
</html>
