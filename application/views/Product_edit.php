<!DOCTYPE html>
<html lang = "en">
<head>
   <meta charset = "utf-8">
   <title>Edición de producto</title>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
  <a href = "<?php echo base_url(); ?>index.php/product">Ver todos</a>
  <hr />

        <?php
        echo form_open('Product/update');
          echo form_hidden('old_id',$old_id);
          echo form_label('Id');
          echo form_input(array('id'=>'txtId',
                                'name'=>'txtId',
                                'value'=>$productList[0]->id));
          echo " ";
          echo form_label('Nombre');
          echo form_input(array('id'=>'txtName',
                                'name'=>'txtName',
                                'value'=>$productList[0]->name));

          echo " ";
          echo form_label('Precio');
          echo form_input(array('id'=>'txtPrice',
                               'name'=>'txtPrice',
                               'value'=>$productList[0]->price));
          echo "";

          echo form_submit(array('id'=>'submit','value'=>'Actualizar'));
        echo form_close();
        ?>

</body>

</html>
