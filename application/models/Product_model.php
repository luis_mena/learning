<?php
  class Product_Model extends CI_model
  {
    function __construct()
    {
      parent::__construct();
    }


    public function save($data)
    {
      if($this->db->insert("products" , $data))
      {
        return true;
      }
     }


     public function delete($id)
     {
         if ($this->db->delete("products", "id = ".$id))
        {
            return true;
         }
      }


      public function update($data,$old_id)
      {
        $this->db->set($data);
        $this->db->where("id", $old_id);
        if ($this->db->update("products", $data))
       {
           return true;
        }
     }


  }
 ?>
