###################
Descripción
###################

El objetivo de este proyecto es realizar un CRUD.
La tabla utilizada es products con las columnas:
id = int
name = string
price = double

*******************
Enlace
*******************
http://localhost/learning/index.php/product


*******************
Inserts de prueba
*******************
insert into products (name,price)VALUES('Producto 1' , 100.00)_
insert into products (name,price)VALUES('Producto 2' , 10.00)_
insert into products (name,price)VALUES('Producto 3' , 25.00)_
insert into products (name,price)VALUES('Producto 4' , 36.00)_
insert into products (name,price)VALUES('Producto 5' , 56.00)_
